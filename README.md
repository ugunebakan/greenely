
# GREENLY APP

## Dependencies:
For this code assesment **Django Framework** is used (Python3).
To obtain REST APIs **Django Rest Framework (DRF)** is used .
**Swagger** is used for documentation of the REST APIs.
**REST framework JWT Auth** is used for JWT authentication.

## Authentication:
There are two types of authentication is available:
	1- JWT for API consumers
	2- Session authentication for Django Admin Panel & Swagger

## Time:

I spent about 4 hours for this project.

## API Docs
Docs are available at http://localhost:8000/docs/

## Application:

- *Dummy users & passwords*
If you access the root of the application you will see 200 users with matches the number of users of the prepared data. So, every single data on the DB has a registered user.

      ....  
	         {  "username":  "user0",  "usable_password":  "qwe123asdA!"  }, 
	         {  "username":  "user1",  "usable_password":  "qwe123asdA!"  }, 
	         {  "username":  "user2",  "usable_password":  "qwe123asdA!"  }, 
	  ....

The user user0 is the superuser. You can access the admin site with this user from [
django admin site](localhost:8000/admin/)

- *Admin Site*
Admins can change such data as users, user groups, day or month from the admin site.

- *Obtain JWT Token*
http://localhost:8000/api-token-auth/ is the end point to get the JWT token.
Only POST and OPTIONS methods are allowed.
Exmple:

POST Data

        {
        "username": "user0",
        "password": "qwe123asdA!"
        }
Response

    {  "token":  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InVzZXIwIiwiZXhwIjoxNTI4OTMxMjE1LCJlbWFpbCI6InVzZXIwQGV4YW1wbGUuY29tIn0.xwxTlqj43MrNKbVqKi8S-jDnJpPdEHEA1hofgZB2EZg"  }

Token should be added to the every request to authorize the user.

    header = {'Authorization: JWT < token >}

- *Get Days or Months Data*
http://localhost:8000/temperature/days/ is the endpoint to list the data

GET Response

    {  "count":  471,  "next":  "[http://localhost:8000/temperature/days/?limit=100&offset=100](http://localhost:8000/temperature/days/?limit=100&offset=100)",  "previous":  null,  "results":  [  {  "day_id":  1,  "timestamp":  "2014-09-01",  "consumption":  6,  "temperature":  5  },  {  "day_id":  2,  "timestamp":  "2014-09-02",  "consumption":  13,  "temperature":  -8  },...

 1. count key is the number of items in this resource
 2. next is the next page for the rest of the data since pagination show only 100 data per page
 3. results is the key to get the data
 4. The only difference between days and months are day_id or month_id.
 5. To access a single entry: http://localhost:8000/temperature/days/{day_id}/ or http://localhost:8000/temperature/months/{month_id}/
 

## Installation On Local Environment:
1- Clone the repo `git clone https://bitbucket.org/ugunebakan/greenely.git`
2- `cd greenely`
3- Copy the example DB in greenely folder
4- `virtualenv env -p python3`
5- `pip install -r requirements.txt`
6- `python manage.py migration`
7- `python manage.py runserver`

## Testing:
1- First 5 steps from installation
2- `python manage.py test`

## Notes:
* Theres is a pipeline available on the repo. It runs the test after each commit.
* Data tests are not available since the DB is predefined.
* Passwords are hashed with SHA256 and salted with 11bit.

## Pagination:
Related pagination documentation can be found from here http://www.django-rest-framework.org/api-guide/pagination/#limitoffsetpagination 
