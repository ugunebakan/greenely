from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    usable_password = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'username',
            'usable_password'
        ]

    def get_usable_password(self, obj):
        return 'qwe123asdA!'
