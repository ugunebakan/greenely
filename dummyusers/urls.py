from rest_framework import routers
from dummyusers.views import UserViewSet


router = routers.SimpleRouter()
router.register(r'', UserViewSet, base_name='users')

urlpatterns = router.urls
