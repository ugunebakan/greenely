from rest_framework import viewsets
from django.contrib.auth import get_user_model
from dummyusers.serializers import UserSerializer
from rest_framework.permissions import AllowAny

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    """
    Dummy user view for you to access the app easyly. user0 is super user and the rest are normal users.
    """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
