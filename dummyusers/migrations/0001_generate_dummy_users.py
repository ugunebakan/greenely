from django.db import migrations
from django.contrib.auth.hashers import make_password


class Migration(migrations.Migration):
    def create_dummy_users(apps, schema_editor):
        # get user model
        User = apps.get_model('auth', 'User')
        # number of user of the given DB
        number_of_users = 200
        # sha256 hashed default password
        password = make_password('qwe123asdA!')
        for x in range(0, number_of_users):

            # user information
            user_info = {
                'email': 'user{0}@example.com'.format(x),
                'is_active': True,
                'is_staff': True,
                'is_superuser': True if x == 0 else False,
                'username': 'user{0}'.format(x),
                'first_name': 'John{0}'.format(x),
                'last_name': 'Doe',
                'password': password
            }

            # create users
            User.objects.create(**user_info)

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.RunPython(create_dummy_users)
    ]
