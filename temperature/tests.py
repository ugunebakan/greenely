from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status
from temperature.models import Day, Month


class InitialGISTestCase(APITestCase):
    """
    Since the DB model is inspected from an existing one, 
    it is impossible to test it against data.
    """

    def setUp(self):
        self.credentials1 = {
            'username': 'testuser1',
            'password': '12345'
        }
        self.credentials2 = {
            'username': 'testuser2',
            'password': '12345'
        }

        self.user1 = User.objects.create_user(**self.credentials1)
        self.user1.is_superuser = True
        self.user1.save()

        self.user2 = User.objects.create_user(**self.credentials2)
        self.user2.is_superuser = True
        self.user2.save()

    def test_get_user_token(self):
        response = self.client.post(
            '/api-token-auth/', self.credentials1, format='json')
        token1 = response.json().get('token')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.post(
            '/api-token-auth/', self.credentials2, format='json')
        token2 = response.json().get('token')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_cannot_access_days_data_without_logging_in(self):
        url = '/temperature/days/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_cannot_access_months_data_without_logging_in(self):
        url = '/temperature/months/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
