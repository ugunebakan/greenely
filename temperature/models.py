from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Day(models.Model):
    day_id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    consumption = models.IntegerField()
    temperature = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'days'


class Month(models.Model):
    month_id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    consumption = models.IntegerField()
    temperature = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'months'
