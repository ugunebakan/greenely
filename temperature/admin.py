from django.contrib import admin
from temperature.models import Day, Month


class DaysAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Day._meta.fields]
    search_fields = list_display


class MonthAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Month._meta.fields]
    search_fields = list_display


admin.site.register(Day, DaysAdmin)
admin.site.register(Month, MonthAdmin)
