from rest_framework import viewsets
from temperature.models import Day, Month
from temperature.serializers import DaySerializer, MonthSerializer


class DayViewSet(viewsets.ModelViewSet):

    queryset = Day.objects.all()
    serializer_class = DaySerializer

    # queyset filter will return the values of request.user
    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(user_id=user.id)


class MonthViewSet(viewsets.ModelViewSet):

    queryset = Month.objects.all()
    serializer_class = MonthSerializer

    # queyset filter will return the values of request.user
    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(user_id=user.id)
