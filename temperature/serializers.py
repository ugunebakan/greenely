from rest_framework import serializers
from temperature.models import Day, Month


class DaySerializer(serializers.ModelSerializer):

    class Meta:
        model = Day
        exclude = [
            'user'
        ]


class MonthSerializer(serializers.ModelSerializer):

    class Meta:
        model = Month
        exclude = [
            'user'
        ]
