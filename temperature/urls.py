from rest_framework import routers
from temperature.views import DayViewSet, MonthViewSet


router = routers.SimpleRouter()
router.register(r'months', MonthViewSet, base_name='months')
router.register(r'days', DayViewSet, base_name='days')

urlpatterns = router.urls
