# Generated by Django 2.0.6 on 2018-06-13 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Day',
            fields=[
                ('day_id', models.IntegerField(primary_key=True, serialize=False)),
                ('timestamp', models.DateTimeField()),
                ('consumption', models.IntegerField()),
                ('temperature', models.IntegerField()),
            ],
            options={
                'db_table': 'days',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Month',
            fields=[
                ('month_id', models.IntegerField(primary_key=True, serialize=False)),
                ('timestamp', models.DateTimeField()),
                ('consumption', models.IntegerField()),
                ('temperature', models.IntegerField()),
            ],
            options={
                'db_table': 'months',
                'managed': False,
            },
        ),
    ]
